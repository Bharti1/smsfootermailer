package in.spice;

import in.spicedigital.configuration.NewConnection;
import in.spicedigital.service.SendEmail;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.dbutils.DbUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

public class ExcelExportMain
{
  private static String source_path = "";
  private static String destination_path = "";
  private static final String SQL_GET_DATA = "SELECT Date_time,TOTALSMSHITS,MHELPFooter,PLATFORMFooter,PNRFooter,SCHEDULEFooter,SEATFooter,SPOTFooter,TKTFooter,TRAINFooter,TSEATFooter,ADFooter,AppFooter,RegFooter,TIMEFooter,MealFooter,ALARMFooter,ALTFooter FROM footer_count WHERE  DATE_FORMAT(DATE_TIME,'%Y-%m-%d')<=DATE_FORMAT(?,'%Y-%m-%d') AND DATE_FORMAT(DATE_TIME,'%Y-%m')=DATE_FORMAT(?,'%Y-%m') ORDER BY DATE_TIME";
  private static final String SQL_GET_TOTAL_DATA = "SELECT sum(TOTALSMSHITS),sum(MHELPFooter),sum(PLATFORMFooter),sum(PNRFooter),sum(SCHEDULEFooter),sum(SEATFooter),sum(SPOTFooter),sum(TKTFooter),sum(TRAINFooter),sum(TSEATFooter),sum(ADFooter),sum(AppFooter),sum(RegFooter),sum(TIMEFooter),sum(MealFooter),sum(ALARMFooter),sum(ALTFooter) FROM footer_count WHERE  DATE_FORMAT(DATE_TIME,'%Y-%m-%d')<=DATE_FORMAT(?,'%Y-%m-%d') AND DATE_FORMAT(DATE_TIME,'%Y-%m')=DATE_FORMAT(?,'%Y-%m')";
  
  public static void main(String[] args)
  {
    Properties prop = new Properties();
    String callDate = args[0];
    try
    {
      prop.load(ExcelExportMain.class.getClassLoader().getResourceAsStream("excel.properties"));
      source_path = prop.getProperty("Source_Path");
      String filename = prop.getProperty("Source_Filename");
      
      source_path = source_path + "/" + filename;
      destination_path = prop.getProperty("Destination_Path");
      
      destination_path = destination_path + "/SMSFooterMIS_" + callDate + ".xls";
      System.out.println(source_path);
      getData(source_path, callDate);
      String To = prop.getProperty("Email_TO");
      String CC = prop.getProperty("Email_CC");
      String mailMessage = "Dear All,\n\nPlease find CTSMSFOOTER MIS for " + callDate + " as attachment.\n\nThanks & Regards,\nSPICE Team.";
      SendEmail.sendMail("CTSMSFOOTER MIS : " + callDate, mailMessage, callDate, To, CC, destination_path);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public static void getData(String path, String callDate)
  {
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    try
    {
      con = NewConnection.getDbConnection();
      FileInputStream fsIP = new FileInputStream(new File(path));
      Map<String, String> map = new HashMap();
      HSSFWorkbook wb = new HSSFWorkbook(fsIP);
      HSSFSheet worksheet = wb.getSheetAt(0);
      





      Cell cell = null;
      
      st = con.prepareStatement("SELECT Date_time,TOTALSMSHITS,MHELPFooter,PLATFORMFooter,PNRFooter,SCHEDULEFooter,SEATFooter,SPOTFooter,TKTFooter,TRAINFooter,TSEATFooter,ADFooter,AppFooter,RegFooter,TIMEFooter,MealFooter,ALARMFooter,ALTFooter,DIAL139Footer FROM footer_count WHERE  DATE_FORMAT(DATE_TIME,'%Y-%m-%d')<=DATE_FORMAT(?,'%Y-%m-%d') AND DATE_FORMAT(DATE_TIME,'%Y-%m')=DATE_FORMAT(?,'%Y-%m') ORDER BY DATE_TIME");
      st.setString(1, callDate);
      st.setString(2, callDate);
      
      rs = st.executeQuery();
      int rownum = 1;
      while (rs.next())
      {
        int columnNum = 0;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getString(1));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(2));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(3));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(4));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(5));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(6));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(7));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(8));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(9));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(10));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(11));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getFloat(12));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getFloat(13));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getFloat(14));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(15));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(16));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(17));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(18));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(19));
        columnNum++;
        rownum++;
      }
      rs.close();
      
      st = con.prepareStatement("SELECT sum(TOTALSMSHITS),sum(MHELPFooter),sum(PLATFORMFooter),sum(PNRFooter),sum(SCHEDULEFooter),sum(SEATFooter),sum(SPOTFooter),sum(TKTFooter),sum(TRAINFooter),sum(TSEATFooter),sum(ADFooter),sum(AppFooter),sum(RegFooter),sum(TIMEFooter),sum(MealFooter),sum(ALARMFooter),sum(ALTFooter),sum(DIAL139Footer) FROM footer_count WHERE  DATE_FORMAT(DATE_TIME,'%Y-%m-%d')<=DATE_FORMAT(?,'%Y-%m-%d') AND DATE_FORMAT(DATE_TIME,'%Y-%m')=DATE_FORMAT(?,'%Y-%m')");
      st.setString(1, callDate);
      st.setString(2, callDate);
      
      rs = st.executeQuery();
      while (rs.next())
      {
        int columnNum = 0;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue("TOTAL");
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(1));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(2));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(3));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(4));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(5));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(6));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(7));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(8));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(9));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(10));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getFloat(11));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getFloat(12));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getFloat(13));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(14));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(15));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(16));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(17));
        columnNum++;
        cell = worksheet.getRow(rownum).getCell(columnNum);
        cell.setCellValue(rs.getInt(18));
        columnNum++;
        rownum++;
      }
      rs.close();
      

      fsIP.close();
      FileOutputStream output_file = new FileOutputStream(new File(destination_path));
      
      wb.write(output_file);
      output_file.close();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      DbUtils.closeQuietly(con);
      DbUtils.closeQuietly(st);
      DbUtils.closeQuietly(rs);
    }
  }
}
