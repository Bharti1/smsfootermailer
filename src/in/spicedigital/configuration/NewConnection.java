package in.spicedigital.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.apache.log4j.Logger;





public class NewConnection {
	static Logger logger = Logger.getLogger(NewConnection.class);
	
	public static Connection getDbConnection(){

		Connection c = null;

		Properties prop = new Properties();

		try {
			prop.load(NewConnection.class.getClassLoader().getResourceAsStream(
					"database.properties"));
			String driver = prop.getProperty("db.driver");

			String url = prop.getProperty("db.url");
			String username = prop.getProperty("db.user");
			String password = prop.getProperty("db.password");

			Class.forName(driver);
			c = DriverManager.getConnection(url, username, password);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		return c;
	}
	
	/*
	 //Connection Pooling Using C3Po
	 * public static Connection getDbConnection(){
		Connection c = null;
		InitialContext context;
		try {
			context = new InitialContext();
			DataSource ds = (DataSource) context.lookup( "java:comp/env/spicesfaDBPool");
			if(ds instanceof PooledDataSource){
				PooledDataSource source = (PooledDataSource)ds;
				c = source.getConnection();
				logger.info("#Location Tracking# DB Connectin tracker log: spicesfaDBPool   "+source.getNumBusyConnectionsDefaultUser()+"/"+source.getNumConnectionsDefaultUser()+"   "+ source.getNumIdleConnectionsDefaultUser());
			}
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return c;
	}*/
}
