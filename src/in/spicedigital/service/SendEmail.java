package in.spicedigital.service;

import in.spice.ExcelExportMain;
import in.spice.util.OsType;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendEmail
{
  private static String PATH = "";
  
  public static void sendMail(String subject, String mail_message, String callDate, String TO, String CC, String fullFilePath)
  {
    String from = "\"SMSFooter MIS\"<smppsupport24x7.r2@digispice.com>";
    

    String curr_date = getDate();
    
    String m_text = mail_message;
    


    Properties props = new Properties();
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.socketFactory.fallback", "true");
    if (OsType.isWindows())
    {
      props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
      
      props.put("mail.smtp.port", "465");
      props.put("mail.smtp.socketFactory.port", "465");
    }
    else if (OsType.isUnix())
    {
      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.port", "587");
      props.put("mail.smtp.socketFactory.port", "587");
    }
    Session session = Session.getInstance(props, new Authenticator()
    {
      protected PasswordAuthentication getPasswordAuthentication()
      {
        return new PasswordAuthentication("smppsupport24x7.r2@digispice.com", "abcd@1234");
      }
    });
    session.setDebug(true);
    System.out.println("PATH for the file" + PATH);
    
    System.out.println("Sending mail......");
    try
    {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(from));
      


      System.out.println("To = " + TO);
      System.out.println("CC = " + CC);
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(TO));
      msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(CC));
      
      msg.setSubject(subject);
      msg.setSentDate(new Date());
      MimeMultipart mp = new MimeMultipart();
      MimeBodyPart text = new MimeBodyPart();
      text.setDisposition("inline");
      text.setContent(m_text, "text/plain");
      mp.addBodyPart(text);
      
      Properties prop = new Properties();
      try
      {
        prop.load(ExcelExportMain.class.getClassLoader().getResourceAsStream("excel.properties"));
      }
      catch (IOException ex)
      {
        Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
      }
      mp = attachFile(mp, fullFilePath);
      
      msg.setContent(mp);
      
      msg.saveChanges();
      
      Transport.send(msg);
    }
    catch (MessagingException mex)
    {
      mex.printStackTrace();
      while (mex.getNextException() != null)
      {
        Exception ex = mex.getNextException();
        ex.printStackTrace();
        if (!(ex instanceof MessagingException)) {
          break;
        }
        mex = (MessagingException)ex;
      }
    }
  }
  
  public static void sendBBPOMail(String subject, String mail_message, String callDate, String TO, String CC, String fullFilePath)
  {
    String from = "\"Railway Feedback MIS\"<passenger.feedback@bharatbpo.in>";
    

    String curr_date = getDate();
    
    String m_text = mail_message;
    


    Properties props = new Properties();
    
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.socketFactory.fallback", "true");
    if (OsType.isWindows())
    {
      props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
      
      props.put("mail.smtp.port", "465");
      props.put("mail.smtp.socketFactory.port", "465");
    }
    else if (OsType.isUnix())
    {
      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.port", "587");
      props.put("mail.smtp.socketFactory.port", "587");
    }
    Session session = Session.getInstance(props, new Authenticator()
    {
      protected PasswordAuthentication getPasswordAuthentication()
      {
        return new PasswordAuthentication("smppsupport24x7.r2@digispice.com", "abcd@1234");
      }
    });
    session.setDebug(true);
    System.out.println("PATH for the file" + PATH);
    
    System.out.println("Sending mail......");
    try
    {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(from));
      


      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(TO));
      msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(CC));
      
      msg.setSubject(subject);
      msg.setSentDate(new Date());
      MimeMultipart mp = new MimeMultipart();
      MimeBodyPart text = new MimeBodyPart();
      text.setDisposition("inline");
      text.setContent(m_text, "text/plain");
      mp.addBodyPart(text);
      
      Properties prop = new Properties();
      try
      {
        prop.load(ExcelExportMain.class.getClassLoader().getResourceAsStream("excel.properties"));
      }
      catch (IOException ex)
      {
        Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
      }
      mp = attachFile(mp, fullFilePath);
      
      msg.setContent(mp);
      
      msg.saveChanges();
      
      Transport.send(msg);
    }
    catch (MessagingException mex)
    {
      mex.printStackTrace();
      while (mex.getNextException() != null)
      {
        Exception ex = mex.getNextException();
        ex.printStackTrace();
        if (!(ex instanceof MessagingException)) {
          break;
        }
        mex = (MessagingException)ex;
      }
    }
  }
  
  public static MimeMultipart attachFile(MimeMultipart mp, String FilePath)
  {
    try
    {
      PATH = FilePath;
      
      System.out.println("Path = " + PATH);
      File file = new File(PATH);
      if (!file.exists())
      {
        System.out.println("SEND MAIL:\tmp:" + mp);
        System.exit(0);
        return mp;
      }
      System.out.println("SEND MAIL:\tAttaching file: ");
      

      MimeBodyPart file_part = new MimeBodyPart();
      FileDataSource fds = new FileDataSource(file);
      DataHandler dh = new DataHandler(fds);
      file_part.setFileName(file.getName());
      file_part.setDisposition("attachment");
      file_part.setDescription("SEND MAIL:\tAttached file: " + file.getName());
      file_part.setDataHandler(dh);
      mp.addBodyPart(file_part);
      return mp;
    }
    catch (Exception e)
    {
      System.out.println("SEND MAIL:\tError while attaching File : ");
    }
    return mp;
  }
  
  public static String getDate()
  {
    String date = "";
    Calendar cal = Calendar.getInstance();
    cal.add(5, -1);
    String month = new SimpleDateFormat("MMM").format(cal.getTime());
    int year = cal.get(1);
    date = month + "_" + year;
    return date;
  }
}
