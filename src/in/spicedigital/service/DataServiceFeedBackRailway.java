package in.spicedigital.service;

import in.spicedigital.configuration.NewConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class DataServiceFeedBackRailway {

    public Map<String, String> getData(String callDate) {

        Connection con = null;
        Map<String, String> map = new HashMap<String, String>();
        try {
            con = NewConnection.getDbConnection();
            String sql = "SELECT train_number,ifnull(total_calls,0),ifnull(call_matured,0),ifnull(stn_clean_good,0),ifnull(stn_clean_satisfactory,0),ifnull(stn_clean_unsatisfactory,0),ifnull(train_clean_good,0),ifnull(train_clean_satisfactory,0),ifnull(train_clean_unsatisfactory,0),ifnull(catring_good,0),ifnull(catring_satisfactory,0),ifnull(catring_unsatisfactory,0) FROM tbl_feedback_mis_modified WHERE DATE_FORMAT(date_time,'%Y-%m-%d') = '" + callDate + "'";
            //String sql = "SELECT train_number,COUNT(1),SUM(IF(call_status='S',1,0)),SUM(IF(q1_input=1,1,0)),SUM(IF(q1_input=2,1,0)),SUM(IF(q1_input=3,1,0)),SUM(IF(q2_input=1,1,0)),SUM(IF(q2_input=2,1,0)),SUM(IF(q2_input=3,1,0)),SUM(IF(q3_input=1,1,0)),SUM(IF(q3_input=2,1,0)),SUM(IF(q3_input=3,1,0)) FROM obd_cdrs where date(call_datetime) like '%"+callDate+"%' GROUP BY train_number";
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                map.put(rs.getString(1),
                        rs.getString(2) + "#" + rs.getString(3) + "#"
                        + rs.getString(4) + "#" + rs.getString(5) + "#"
                        + rs.getString(6) + "#" + rs.getString(7) + "#"
                        + rs.getString(8) + "#" + rs.getString(9) + "#"
                        + rs.getString(10) + "#" + rs.getString(11)
                        + "#" + rs.getString(12));
            }

        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public Map<String, String> getTrainData_StationCleanliness(String train_no, String callDate) {
        Connection con = null;
        Map<String, String> map = new HashMap<String, String>();
        try {
            con = NewConnection.getDbConnection();
            //String sql = "SELECT station_code,SUM(IF(class='1A',1,0)) '1A',SUM(IF(class='2A',1,0)) '2A',SUM(IF(class='3A',1,0)) '3A',SUM(IF(class='SL',1,0)) 'SL',SUM(IF(q1_input=1 AND class='1A',1,0)),SUM(IF(q1_input=2 AND class='1A',1,0)),SUM(IF(q1_input=3 AND class='1A',1,0)),SUM(IF(q1_input=1 AND class='2A',1,0)),SUM(IF(q1_input=2 AND class='2A',1,0)),SUM(IF(q1_input=3 AND class='2A',1,0)),SUM(IF(q1_input=1 AND class='3A',1,0)),SUM(IF(q1_input=2 AND class='3A',1,0)),SUM(IF(q1_input=3 AND class='3A',1,0)),SUM(IF(q1_input=1 AND class='SL',1,0)),SUM(IF(q1_input=2 AND class='SL',1,0)),SUM(IF(q1_input=3 AND class='SL',1,0))FROM obd_cdrs WHERE train_number='"+train_no+"' AND call_status='S' AND station_code IS NOT NULL AND DATE(call_datetime) LIKE '%"+callDate+"%' GROUP BY station_code";
            String sql = "SELECT stn_code,ifnull(1AC_calls,0),ifnull(2AC_calls,0),ifnull(3AC_calls,0),ifnull(SL_calls,0),ifnull(1AC_good,0),ifnull(1AC_satisfactory,0),ifnull(1AC_unsatisfactory,0),ifnull(2AC_good,0),ifnull(2AC_satisfactory,0),ifnull(2AC_unsatisfactory,0),ifnull(3AC_good,0),ifnull(3AC_satisfactory,0),ifnull(3AC_unsatisfactory,0),ifnull(SL_good,0),ifnull(SL_satisfactory,0),ifnull(SL_unsatisfactory,0) FROM tbl_stationwise_feedback_mis_modified WHERE train_no='" + train_no + "' AND DATE_FORMAT(date_time,'%Y-%m-%d')='" + callDate + "'";
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                map.put(rs.getString(1),
                        rs.getString(2) + "#" + rs.getString(3) + "#"
                        + rs.getString(4) + "#" + rs.getString(5) + "#"
                        + rs.getString(6) + "#" + rs.getString(7) + "#"
                        + rs.getString(8) + "#" + rs.getString(9) + "#"
                        + rs.getString(10) + "#" + rs.getString(11)
                        + "#" + rs.getString(12) + "#" + rs.getString(13)
                        + "#" + rs.getString(14) + "#" + rs.getString(15)
                        + "#" + rs.getString(16) + "#" + rs.getString(17));
            }
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public Map<String, String> getTotalinitiatedCalls(String train_no, String callDate) {
        Map<String, String> callDetails = new HashMap<String, String>();
        Connection con = null;
        try {
            con = NewConnection.getDbConnection();
            String sql = "select ifnull(total_calls,0),ifnull(call_matured,0) from tbl_feedback_mis where train_number ='" + train_no + "' and date_format(date_time,'%Y-%m-%d') = '" + callDate + "'";
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                //System.out.println("Train No = "+train_no+"Total Calls == "+rs.getString(1)+" success Calls == "+rs.getString(2));
                callDetails.put("0", rs.getString(1) + "#" + rs.getString(2));
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return callDetails;
    }

    public Map<String, String> getTrainData_TrainCleanliness(String train_no, String callDate) {

        Connection con = null;
        Map<String, String> map = new HashMap<String, String>();

        try {

            con = NewConnection.getDbConnection();
            String sql = "SELECT '0',ifnull(1AC_good+1AC_sat+1AC_usat,0),ifnull(2AC_good+2AC_sat+2AC_unsat,0),ifnull(3AC_good+3AC_sat+3AC_unsat,0),ifnull(SL_good+SL_sat+SL_unsat,0),ifnull(1AC_good,0),ifnull(1AC_sat,0),ifnull(1AC_usat,0),ifnull(2AC_good,0),ifnull(2AC_sat,0),ifnull(2AC_unsat,0),ifnull(3AC_good,0),ifnull(3AC_sat,0),ifnull(3AC_unsat,0),ifnull(SL_good,0),ifnull(SL_sat,0),ifnull(SL_unsat,0) FROM tbl_train_feedback_mis_modified WHERE feedback_type ='Train Cleanliness' AND train_no='" + train_no + "' AND date_format(date_time,'%Y-%m-%d')='" + callDate + "'";
            //String sql = "SELECT '0','0','0','0','0',SUM(IF(q2_input=1 AND class='1A',1,0)),SUM(IF(q2_input=2 AND class='1A',1,0)),SUM(IF(q2_input=3 AND class='1A',1,0)),SUM(IF(q2_input=1 AND class='2A',1,0)),SUM(IF(q2_input=2 AND class='2A',1,0)),SUM(IF(q2_input=3 AND class='2A',1,0)),SUM(IF(q2_input=1 AND class='3A',1,0)),SUM(IF(q2_input=2 AND class='3A',1,0)),SUM(IF(q2_input=3 AND class='3A',1,0)),SUM(IF(q2_input=1 AND class='SL',1,0)),SUM(IF(q2_input=2 AND class='SL',1,0)),SUM(IF(q2_input=3 AND class='SL',1,0))FROM obd_cdrs WHERE train_number='"+train_no+"' AND call_status='S' and date(call_datetime) like '%"+callDate+"%'";
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                map.put("0",
                        rs.getString(2) + "#" + rs.getString(3) + "#"
                        + rs.getString(4) + "#" + rs.getString(5) + "#"
                        + rs.getString(6) + "#" + rs.getString(7) + "#"
                        + rs.getString(8) + "#" + rs.getString(9) + "#"
                        + rs.getString(10) + "#" + rs.getString(11)
                        + "#" + rs.getString(12) + "#" + rs.getString(13)
                        + "#" + rs.getString(14) + "#" + rs.getString(15)
                        + "#" + rs.getString(16) + "#" + rs.getString(17));

            }

            System.out.println(map + " values in map");

        } catch (Exception e) {
            e.getStackTrace();

        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return map;
    }

    public Map<String, String> getTrainData_QualityofCatering(String train_no, String halt_station, String callDate) {

        Connection con = null;
        Map<String, String> map = new HashMap<String, String>();

        try {

            con = NewConnection.getDbConnection();
            String sql = "select '0',ifnull(1AC_good+1AC_sat+1AC_usat,0),ifnull(2AC_good+2AC_sat+2AC_unsat,0),ifnull(3AC_good+3AC_sat+3AC_unsat,0),ifnull(SL_good+SL_sat+SL_unsat,0),ifnull(1AC_good,0),ifnull(1AC_sat,0),ifnull(1AC_usat,0),ifnull(2AC_good,0),ifnull(2AC_sat,0),ifnull(2AC_unsat,0),ifnull(3AC_good,0),ifnull(3AC_sat,0),ifnull(3AC_unsat,0),ifnull(SL_good,0),ifnull(SL_sat,0),ifnull(SL_unsat,0) FROM tbl_train_feedback_mis_modified WHERE feedback_type ='Catering Services' AND train_no='" + train_no + "' AND date_format(date_time,'%Y-%m-%d')='" + callDate + "'";
            //String sql = "SELECT 0,0,0,0,0,SUM(IF(q3_input=1 AND class='1A',1,0)),SUM(IF(q3_input=2 AND class='1A',1,0)),SUM(IF(q3_input=3 AND class='1A',1,0)),SUM(IF(q3_input=1 AND class='2A',1,0)),SUM(IF(q3_input=2 AND class='2A',1,0)),SUM(IF(q3_input=3 AND class='2A',1,0)),SUM(IF(q3_input=1 AND class='3A',1,0)),SUM(IF(q3_input=2 AND class='3A',1,0)),SUM(IF(q3_input=3 AND class='3A',1,0)),SUM(IF(q3_input=1 AND class='SL',1,0)),SUM(IF(q3_input=2 AND class='SL',1,0)),SUM(IF(q3_input=3 AND class='SL',1,0))FROM obd_cdrs WHERE train_number='"+train_no+"' AND call_status='S' and date(call_datetime) like '%"+callDate+"%'";
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                map.put("0",
                        rs.getString(2) + "#" + rs.getString(3) + "#"
                        + rs.getString(4) + "#" + rs.getString(5) + "#"
                        + rs.getString(6) + "#" + rs.getString(7) + "#"
                        + rs.getString(8) + "#" + rs.getString(9) + "#"
                        + rs.getString(10) + "#" + rs.getString(11)
                        + "#" + rs.getString(12) + "#" + rs.getString(13)
                        + "#" + rs.getString(14) + "#" + rs.getString(15)
                        + "#" + rs.getString(16) + "#" + rs.getString(17));

            }

        } catch (Exception e) {
            e.getStackTrace();

        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return map;
    }

    public String getStationCode(String train_no, String callDate) {

        Connection con = null;
        String station_code = "";

        try {

            con = NewConnection.getDbConnection();
            //	String sql = "SELECT DISTINCT(station_code) FROM train_schedule WHERE train_number='"+train_no+"'";
            String sql = "SELECT station_code FROM obd_cdrs WHERE train_number='" + train_no + "' AND station_code IS NOT NULL AND DATE(call_datetime) LIKE '%" + callDate + "%' GROUP BY station_code ";
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                station_code += rs.getString(1) + "#";
            }

        } catch (Exception e) {
            e.getStackTrace();

        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return station_code;
    }
}
